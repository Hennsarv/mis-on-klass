﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnKlass
{
    class Program
    {
        static void Main(string[] args)
        {
            // lükkame sagedamini muudatusi üles

            Inimene i4 = new Inimene("henn", new DateTime(1955,3,7), Gender.Mees ); // kasutab 3 parameetriga konstruktorit
            Inimene i5 = new Inimene("ants", DateTime.Parse("1968-11-20")); // kasutab 2 parameetriga konstruktorit
            Console.WriteLine(i5.Id); // kui propertil on vaid get-meetod, siis on see readonly property
            //Console.WriteLine( Inimene.Inimesed[2]  );

            Console.WriteLine( i4.GetNimi());   // getteri kasutamine
            Console.WriteLine( i4.Nimi);    // property kasutamine

            i4.Nimi = "sarvik";    // property muutmine
            i4.SetNimi("luarvik");   // setteri kasutamine

            new Inimene();


            foreach (var x in Inimene.Inimesed.Values) Console.WriteLine(x);

          

        }

    }

    enum Gender { Naine, Mees, Tundmatu = 99}
    class Inimene
    {
        // staatilised muutujad (juurdepääsu järjekorras)
        private static int nextID = 0;
        public static Dictionary<int, Inimene> Inimesed = new Dictionary<int, Inimene>();



        // instantsi muutujad (mittestaatilised)
        // juurdepääsu järjekorras (private, protected, internal, public)

        private int _Id;
        private string _Nimi;
        private DateTime SünniAeg;

        private Gender _Sugu = Gender.Tundmatu;

        // konstruktorid
        public Inimene(string nimi, DateTime sünniaeg)
        {
            Nimi = nimi; SünniAeg = sünniaeg;
            _Id = ++nextID;
            Inimesed.Add(_Id, this);
        }

        // seotud konstruktorid - kolme pareameetriga new teeb sama, mis kahega
        // ja lisaks midagi veidike veel
        public Inimene(string nimi, DateTime sünniaeg, Gender sugu) 
            : this(nimi, sünniaeg)
        {
            _Sugu = sugu;
        }

        public Inimene() : this("tundmatu", DateTime.Now.Date)
        {

        }


        // propertid ja getterid-setterid
        public int Id { get => this._Id;  }  // getter for ID

        public string GetNimi() { return this._Nimi; } // getter for Nimi

        public void SetNimi(string uusNimi)  // setter for Nimi
        {
            this._Nimi = uusNimi.Substring(0, 1).ToUpper()
                + uusNimi.Substring(1).ToLower();
        }

        public string Nimi
        {
            get { return _Nimi; }
            
            set {
                this._Nimi = value.Substring(0, 1).ToUpper()
              + value.Substring(1).ToLower();
            }
        }

        public int Vanus
        {
            get
            {
                int vanus = DateTime.Now.Year - SünniAeg.Year;
                if (SünniAeg.AddYears(vanus) > DateTime.Now.Date) return vanus - 1;
                else return vanus;
            }
        }

        internal Gender Sugu { get => _Sugu;
            set
            {
                if (this._Sugu == Gender.Tundmatu)
                _Sugu = value;
            }
        }


        // staatilised meetodid

        // muud instantsi meetodid        


        // overrided meetodid
        public override string ToString()
        {
            //return $"{(Sugu==Gender.Mees ? "mees: " : Sugu==Gender.Naine ? "naine: " : "")}{Nimi} vanusega {Vanus:00}";
            return string.Format("Nr.{3} {0}{1} vanusega {2:00}",
                Sugu == Gender.Mees ? "mees: " : Sugu == Gender.Naine ? "naine: " : "",
                _Nimi, Vanus, _Id);

            //return (Sugu == Gender.Mees ? "mees: " : Sugu == Gender.Naine ? "naine: " : "")
            //    + Nimi + " vanusega " + Vanus.ToString("00");


        }
    }


}
